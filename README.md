# OpenArena 0.8.8-stigmha

This is the GPLv2+ source for the game code and engine of OpenArena (OA) 0.8.8. It is a merge of the *0.8.8 Engine Source* and *0.8.8 game source* taken from the [official OpenArena website](http://openarena.ws/page.php?14).

My general vision is to have all code in one place, making it fully debuggable on all supported platforms and IDEs. The current revision of this repository allows you to build the latest 0.8.8 OA QVMs as DLLs in Visual Studio 2013 and to debug them with the visual debugger along with the engine. The Windows QVM scripts are also included in order to build and implement production ready *\*.qvms* of the virtual machines.

**NB: currently Windows and Visual Studio 2013 only**. Support for MinGW, OSX and Linux should be implemented soon. See [my forum post](http://openarena.ws/board/index.php?topic=4900.0) for progress, suggestions and general feedback.

Installation (Windows)
======================

##Download and install

1. [Download and unzip](http://openarena.ws/request.php?4) OpenArena v.0.8.8, for example in the "C:\\Program Files (x86)\\" directory. Remember the location of your install, it should be a permanent location (*i.e. not Desktop or Downloads*)!
	* Visit the [official website's download section](http://openarena.ws/download.php) for an overview of all downloads if the previous link gives you a newer version or an error.
2. Retrieve and install a copy of *Microsoft Visual Studio 2013* with Visual C++. *Visual Studio Express 2013 for Windows Desktop* can be downloaded for free at the official [*Visual Studio* website](http://www.visualstudio.com/downloads/download-visual-studio-vs#d-express-windows-desktop).
3. [Download and unzip](http://libsdl.org/release/SDL-devel-1.2.15-VC.zip) SDL Visual C++ development libraries v.1.2.15 from the official SDL website. It should be unzipped in a permanent location (*i.e. not Desktop or Downloads*) like the "C:\\Program Files (x86)\\" directory.
4. Enter the SDL directory and locate *lib\\x86\\SDL.dll*. Copy the dll and paste it in a location that is stored in your *Windows Environment variable* called "*Path*". "C:\\windows\\system" is a good place, this will make the SDL DLL available when running the solution.
5. Clone this repository to your local machine: `git clone <repo url>`, wait for it to complete.

##Configure Visual Studio

1. Enter the following directory in the repository: "misc\msvc\openarena", and open "openarena.sln". Run through the first-time setup of Visual Studio.
2. From the top menu select "View->Property Manager". The location of this option depends on your own custom Visual Studio configuration, and might be placed in "View->Other Windows->Property Manager" instead.
	* The *Property Manager* on the middle-left or middle-right and looks really similar to the default *Solution Explorer*, just look at the bottom tabs to make sure you're actually in the *Property Manager tab*.
3. Expand one of the listed projects, which one you chose doesn't matter but will expand *openarena* in this case.
4. Expand "Debug | Win32" and right click the "Microsoft.Cpp.Win32.user" configuration that appears.
5. Select "Properties" in the context menu.
6. Select "Common Properties->User Macros" in the left frame of the window that popped up (Windows Property Page).
7. Click the "Add Macro" button.
8. Enter "SDL_1215_PATH" in the "Name" field, and enter the path to your SDL installation in the "Value" field (e.g.: "C:\\Program Files (x86)\\SDL-1.2.15").
9. Check the checkbox next to "Set this macro as an environment variable in the build environment"
10. Click "OK"
11. Click the "Add Macro" button again.
12. Enter "BASEOA_PATH" in the "Name" field, and enter the path to your OpenArena installation in the "Value" field (e.g.: "C:\\Program Files (x86)\\openarena-0.8.8").
13. Check the checkbox next to "Set this macro as an environment variable in the build environment"
14. Click "OK", and then click "OK" in the "Property Pages" window.

*Tip:* you might want to enable line numbers. Click "Tools" in the top menu and select "Options..." from the drop-down. Find and expand "Text Editor" on the left and click/highlight "All Languages". Check "Line numbers" under the "Settings" and make sure it's actually checked and not a filled square. Click "OK". 

##Build and run

1. Make sure you are (back) in "Solution Explorer" (not "Property Manager") in the middle-left or middle-right frame. Confirm it by ensuring that the bottom "Solution Explorer" tab is highlighted.
2. Right click the top element entitled "Solution 'openarena' (4 projects)" and click "Build Solution". Press F7 alternatively. Wait for it to complete, ensure you see "========== Build: 4 succeeded, 0 failed ...".
	* If someone failed, click "View" in the top menu and select "Error List" from the dropdown. There are probably three errors here, stating something like this: `Error 3 error LNK1104: cannot open file 'C:\\Program Files (x86)\\openarena-0.8.8\\baseoa\\\*.dll'..."
	* It means that you have stored your OpenArena install in a location requiring administrator privileges in order to write.
	* Fix it by right clicking your OA install directory (e.g.: "C:\\Program Files (x86)\\openarena-0.8.8") in "Windows Explorer" and select "Properties". Click the "Security" tab and find and press the "Edit..." button. Select "Users" in the first "Group or username:" field. Check the two checkboxes next to "Edit" and "Write" under "Permissions for Users".  Click "OK", and then "OK" again in the "Properties" window.
	* Fix it alternatively by moving your OA install somewhere where your regular user can write to it and update the "BASEOA_PATH" user macro.
	* Enter Visual Studio and attempt to build it  again.
3. The three QVM projects (cgame, game and q3_ui) are built as DLLs and stored directly in the baseoa folder of your Open Arena install directory (e.g.: "C:\\Program Files (x86)\\openarena-0.8.8\\baseoa"). The engine is built and stored in "build\\openarena_debug" directory in the repository's directory.
4. The Visual Studio project is configured to give the engine the proper arguments in order to load the DLLs on launch. Press "F5" or click the Play looking button to run.
	* If you get an error stating that SDL.dll is unavailable, it means that you haven't placed your DLL in a location that is present in the "Windows Environment variable" called "Path". The quick fix is to copy the *64-bit* SDL.dll (in the x64 SDL\\lib dir) to "C:\\Windows\\System32" and copy the 32-bit *SDL.dll* (in the x86 SDL\\lib dir) to "C:\\Windows\\SysWOW64". Move the 32-bit DLL to "System32" if you only have a 32-bit edition of Windows.
	* An alternate fix is to add the current path of your SDL.dll to the *Path* environment variable. Open "Windows Explorer" (Cmd+E), right click "My Computer", click "Advanced system settings" in the left menu and press the "Environment variables..." button. Find and mark the "Path" variable in the bottom form (under "System variables"), and click the "Edit..." button. Add a semi colon (;) to the "Variable value" field followed by the path to the SDL DLL, e.g.: ";C:\Windows\System". Press "OK" three times to close all the popups and to store your changes. You might have to log out of Windows for the changes to take effect.
5. You are now able to set breakpoints and to debug the engine and all the QVMs from Visual Studio.

Building QVMs
=============

The \*.dll/\*.so solution to the QVMs is great for debugging and development, but the engine is intended to work on \*.qvm files packed in \*.pk3 files for the release version of the game.

##Windows

Enter the "qvms/windows_scripts" from the root directory of the repository. Double click the various \*.bat files to build the various QVMs. These build procedures use the same sources as the Visual Studio project. Compiled virtual machines will be stored in "\\qvms\\windows\\baseoa\\vm" once a script is done executing. Use a terminal/cmd to filter out the error messages on failure, e.g.: `.\windows_compile_cgame.bat > output`.

Implement QVMs in the game
==========================

Use [7-zip File Manager](http://7-zip.org/) or another file archiver with *zip* support to open the *pak6-patch088.pk3* file in the baseoa directory (e.g.: "C:\\Program Files (x86)\\openarena-0.8.8\\baseoa\\pak6-patch088.pk3"). \*.pk3 files are renamed zip files. Use the file manager to replace the QVMs in the *vm* subdirectory of the pk3 file with your new qvms. It can alternatively be done by creating a new zip-file containing a subdirectory called *vm* with your QVMs within. Rename the file extension to pk3 and place it in the baseoa directory. You can then launch your changes using the mission packs option in the main menu of the game. 

Other scripts
=============

There are two bat scripts and one config file of interest located in the /misc directory. None of the scripts are required to use, but they simplify the launch of both a client and a dedicated server. **NB:** you have to build the solution in Visual Studio before you use these.

The "local_launch_client.bat" is a script to launch a regular client configured to use and load the DLL files that are produced by the QVMs when building them in Visual Studio.

The "dedicated_server.bat" script copies the dedicated_server.cfg to the *baseoa* directory and launches the engine as a dedicated server configured by the configuration file. It is also set to use the DLLs from Visual Studio, rather than the QVMs. The configuration starts a regular deathmatch with map rotation.

Feel free to modify these three files after your own need.

Changelog
=========

*23.01.2014*

* Created a new repository and Visual Studio solution
* Added the engine source from the official [OA source downloads](http://openarena.ws/page.php?6) to the VS project.
* Added the QVM sources from the official [OA source downloads](http://openarena.ws/page.php?6) to the VS project.
* Made a git repo for the project with a .gitignore file.
* Modified the Windows QVM build scripts to work with the new infrastructure of the project.
* Added the Windows bat scripts to launch a local client and a dedidicated server
* Fixed some double to float truncation warnings in the QVMs
* Fixed a build error in the engine caused by a deprecated Win32 API call (IsWindowsVersionOrGreater()) that checked if the client was Windows NT or older. The new replacement call in the Windows API requires the C++ compiler, so I removed the check entirely now assuming that the user has Windows XP or later. **NB:** This means that Windows NT and lower is no longer supported!
* Added a patch setting video driver to "windib" for debug builds in Sys_GLimpInit() in sys_win32.c line 693. It was originally set to "directx" which actually enforces SDL to use the DirectX v.5 API. This caused a dead-lock like behavior when setting a breakpoint in Visual Studio, resulting in a freezed cursor and an unstable Visual Studio. It freezed the entire machine on OSX hardware.
* Upgraded the SDL code to SDL 1.2.15 (was 1.2.14), because why not? It works perfectly well with the engine and includes [several bugfixes](http://www.libsdl.org/release/changes-1.2.html).
* Upgraded the cURL code to 1.34.1 (was 1.30.4), same reason as with SDL. Contains [many of fixes](http://curl.haxx.se/changes.html) including patching multiple security vulnerabilities.
* Fixed the mix of various newline encodings in q_shared.c
* common.c line 2725, added an #ifdef to remove the "didn't exit properly" prompt from Debug builds as it is annoying when you are developing the engine; you expect it to crash, many times!
* Added three new configuration options to the engine; *com_virtualClient*, *com_virtualCliServer*, *com_virtualCliSvPort*. They can be set using the configuration names; *virtualClient*, *virtualCliServer*, *virtualCliSvPort*. It is the first part of my thesis work on the engine and currently allows a client to bypass the main menu and jump straight into a game. You can test it by launching a dedicated server using the bat script in the /misc directory and then launch a client like this: `.\openarena +set fs_basepath <baseoa path> +set virtualClient 1 +set virtualCliServer localhost +set virtualCliSvPort 27960`. I will continue on this development within a new branch.
* Created this README documentation

Todo / wish list
================

* Create a simple and general *nix Makefile to build the engine that should be as platform independent as possible. It should support MinGW, Linux and OSX.
* Create/modify a simple and general *nix Makefile to build the QVMs as dynamic/shared libraries (\*.dll/\*.so). Should remain platform independent.
* Create/modify a simple and general *nix Makefile to build the QVMs as \*.qvm files. Should remain platform independent.
* Upgrade libspeex?

External Resources
==================

* [OpenArena Developer FAQ](http://openarena.wikia.com/wiki/DeveloperFAQ#Coding)
* [OpenArena Developer Wiki](http://openarena.wikia.com/wiki/Main_Page#Development)
* [OpenArena Forums](http://openarena.ws/board/index.php)
* ["Official" forum post for this repo](http://openarena.ws/board/index.php?topic=4900.0)
* [ioquake3 help section](http://ioquake3.org/help/) (base engine)
* [ioquake3 git repo and readme](https://github.com/ioquake/ioq3/)
* [Open Arena's download section](http://openarena.ws/download.php)
* [Download Visual Studio 2013 Express](http://www.visualstudio.com/downloads/download-visual-studio-vs#d-express-windows-desktop)
* [Official SDL website](http://libsdl.org/)
* [7-zip File Manager](http://7-zip.org/)
* [Fabien Sanglard's excellent code review of the Quake 3 engine](http://fabiensanglard.net/quake3/index.php)
* [Jean-Paul van Waveren's thesis on implementing bots in Quake 3](http://fd.fabiensanglard.net/quake3/The-Quake-III-Arena-Bot.pdf)
