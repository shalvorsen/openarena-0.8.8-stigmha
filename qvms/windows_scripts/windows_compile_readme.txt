2009-04-27 Sago007
2014-01-22 Stigmha

You can now compile the OpenArena mod SDK in Windows!

Use the batch files for baseoa:
 - windows_compile_game.bat
 - windows_compile_cgame.bat
 - windows_compile_q3_ui.bat

Following for missionpack:
 - windows_compile_game_missionpack.bat
 - windows_compile_cgame_missionpack.bat
 - windows_compile_ui_missionpack.bat

The qvm files will be placed in:
 qvms\windows\baseoa\vm
or
 qvms\windows\missionpack\vm

The batch files has no error checking so you need to read the output. Feel free to improve the scripts.

The binary blobs:
 lcc.exe
 q3cpp.exe
 q3rpp.exe
 q3asm.exe

id Software provided both the source and precompiled binaries so I guess they knew how hard these files are to compile. If you succeed in compiling them please tell us how on http://www.openarena.ws/board/. We would really appreciate if anyone can get ioquake3's improved QVM tools compiled as those provided here has some bugs.
