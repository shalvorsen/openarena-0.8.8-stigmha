cd ..
mkdir windows
mkdir windows\build
mkdir windows\baseoa
mkdir windows\baseoa\vm
mkdir windows\build\q3_ui
copy /Y windows_scripts\lcc.exe  windows\build\q3_ui\
copy /Y windows_scripts\q3cpp.exe  windows\build\q3_ui\
copy /Y windows_scripts\q3rcc.exe  windows\build\q3_ui\
copy /Y windows_scripts\q3asm.exe  windows\build\q3_ui\
copy /Y windows_scripts\q3_ui.q3asm windows\build\

set LIBRARY=
set INCLUDE=
set CODEDIR=..\..\..\..\code

set cc=lcc -DQ3_VM -S -Wf-target=bytecode -Wf-g -I%CODEDIR%\q3_ui -I%CODEDIR%\qcommon -I%CODEDIR%\game %1

cd windows\build\q3_ui

%cc%  %CODEDIR%\q3_ui\ui_addbots.c
%cc%  %CODEDIR%\q3_ui\ui_atoms.c
%cc%  %CODEDIR%\q3_ui\ui_cdkey.c
%cc%  %CODEDIR%\q3_ui\ui_challenges.c
%cc%  %CODEDIR%\q3_ui\ui_cinematics.c
%cc%  %CODEDIR%\q3_ui\ui_confirm.c
%cc%  %CODEDIR%\q3_ui\ui_connect.c
%cc%  %CODEDIR%\q3_ui\ui_controls2.c
%cc%  %CODEDIR%\q3_ui\ui_credits.c
%cc%  %CODEDIR%\q3_ui\ui_demo2.c
%cc%  %CODEDIR%\q3_ui\ui_display.c
%cc%  %CODEDIR%\q3_ui\ui_firstconnect.c
%cc%  %CODEDIR%\q3_ui\ui_gameinfo.c
%cc%  %CODEDIR%\q3_ui\ui_ingame.c
%cc%  %CODEDIR%\q3_ui\ui_loadconfig.c
rem %cc%  %CODEDIR%\q3_ui\ui_login.c
%cc%  %CODEDIR%\q3_ui\ui_main.c
%cc%  %CODEDIR%\q3_ui\ui_menu.c
%cc%  %CODEDIR%\q3_ui\ui_mfield.c
%cc%  %CODEDIR%\q3_ui\ui_mods.c
%cc%  %CODEDIR%\q3_ui\ui_network.c
%cc%  %CODEDIR%\q3_ui\ui_options.c
%cc%  %CODEDIR%\q3_ui\ui_password.c
%cc%  %CODEDIR%\q3_ui\ui_playermodel.c
%cc%  %CODEDIR%\q3_ui\ui_players.c
%cc%  %CODEDIR%\q3_ui\ui_playersettings.c
%cc%  %CODEDIR%\q3_ui\ui_preferences.c
%cc%  %CODEDIR%\q3_ui\ui_qmenu.c
rem %cc%  %CODEDIR%\q3_ui\ui_rankings.c
rem %cc%  %CODEDIR%\q3_ui\ui_rankstatus.c
%cc%  %CODEDIR%\q3_ui\ui_removebots.c
%cc%  %CODEDIR%\q3_ui\ui_saveconfig.c
%cc%  %CODEDIR%\q3_ui\ui_serverinfo.c
%cc%  %CODEDIR%\q3_ui\ui_servers2.c
%cc%  %CODEDIR%\q3_ui\ui_setup.c
rem %cc%  %CODEDIR%\q3_ui\ui_signup.c
%cc%  %CODEDIR%\q3_ui\ui_sound.c
%cc%  %CODEDIR%\q3_ui\ui_sparena.c
rem %cc%  %CODEDIR%\q3_ui\ui_specifyleague.c
%cc%  %CODEDIR%\q3_ui\ui_specifyserver.c
%cc%  %CODEDIR%\q3_ui\ui_splevel.c
%cc%  %CODEDIR%\q3_ui\ui_sppostgame.c
%cc%  %CODEDIR%\q3_ui\ui_spreset.c
%cc%  %CODEDIR%\q3_ui\ui_spskill.c
%cc%  %CODEDIR%\q3_ui\ui_startserver.c
%cc%  %CODEDIR%\q3_ui\ui_team.c
%cc%  %CODEDIR%\q3_ui\ui_teamorders.c
%cc%  %CODEDIR%\q3_ui\ui_video.c
%cc%  %CODEDIR%\q3_ui\ui_votemenu.c
%cc%  %CODEDIR%\q3_ui\ui_votemenu_fraglimit.c
%cc%  %CODEDIR%\q3_ui\ui_votemenu_custom.c
%cc%  %CODEDIR%\q3_ui\ui_votemenu_gametype.c
%cc%  %CODEDIR%\q3_ui\ui_votemenu_kick.c
%cc%  %CODEDIR%\q3_ui\ui_votemenu_map.c
%cc%  %CODEDIR%\q3_ui\ui_votemenu_timelimit.c

copy  %CODEDIR%\q3_ui\ui_syscalls.asm ..

%cc%  %CODEDIR%\game\bg_lib.c
%cc%  %CODEDIR%\game\bg_misc.c

%cc%  %CODEDIR%\qcommon\q_math.c
%cc%  %CODEDIR%\qcommon\q_shared.c

q3asm -f ..\q3_ui

cd ..\..\..\windows_scripts
pause
