cd ..
mkdir windows
mkdir windows\build
mkdir windows\missionpack
mkdir windows\missionpack\vm
mkdir windows\build\cgame
copy windows_scripts\lcc.exe  windows\build\cgame\
copy windows_scripts\q3cpp.exe  windows\build\cgame\
copy windows_scripts\q3rcc.exe  windows\build\cgame\
copy windows_scripts\q3asm.exe  windows\build\cgame\
copy windows_scripts\cgame_mp.q3asm windows\build\cgame.q3asm

set LIBRARY=
set INCLUDE=
set CODEPATH=..\..\..\..\code

set cc=lcc -DMISSIONPACK -DQ3_VM -S -Wf-target=bytecode -Wf-g -I%CODEPATH%\cgame -I%CODEPATH%\ui -I%CODEPATH%\qcommon -I%CODEPATH%\game %1

cd windows\build\cgame

%cc%  %CODEPATH%\cgame\cg_challenges.c
%cc%  %CODEPATH%\cgame\cg_consolecmds.c
%cc%  %CODEPATH%\cgame\cg_draw.c
%cc%  %CODEPATH%\cgame\cg_drawtools.c
%cc%  %CODEPATH%\cgame\cg_effects.c
%cc%  %CODEPATH%\cgame\cg_ents.c
%cc%  %CODEPATH%\cgame\cg_event.c
%cc%  %CODEPATH%\cgame\cg_info.c
%cc%  %CODEPATH%\cgame\cg_localents.c
%cc%  %CODEPATH%\cgame\cg_main.c
%cc%  %CODEPATH%\cgame\cg_marks.c
%cc%  %CODEPATH%\cgame\cg_newdraw.c
%cc%  %CODEPATH%\cgame\cg_particles.c
%cc%  %CODEPATH%\cgame\cg_players.c
%cc%  %CODEPATH%\cgame\cg_playerstate.c
%cc%  %CODEPATH%\cgame\cg_predict.c
%cc%  %CODEPATH%\cgame\cg_scoreboard.c
%cc%  %CODEPATH%\cgame\cg_servercmds.c
%cc%  %CODEPATH%\cgame\cg_snapshot.c
%cc%  %CODEPATH%\cgame\cg_unlagged.c
%cc%  %CODEPATH%\cgame\cg_view.c
%cc%  %CODEPATH%\cgame\cg_weapons.c

%cc%  %CODEPATH%\game\bg_lib.c
%cc%  %CODEPATH%\game\bg_misc.c
%cc%  %CODEPATH%\game\bg_pmove.c
%cc%  %CODEPATH%\game\bg_slidemove.c

copy  %CODEPATH%\cgame\cg_syscalls.asm ..

%cc%  %CODEPATH%\qcommon\q_math.c
%cc%  %CODEPATH%\qcommon\q_shared.c

%cc%  %CODEPATH%\ui\ui_shared.c

q3asm -f ..\cgame

cd ..\..\..\windows_scripts
pause
