cd ..
mkdir windows
mkdir windows\build
mkdir windows\missionpack
mkdir windows\missionpack\vm
mkdir windows\build\ui
copy windows_scripts\lcc.exe  windows\build\ui\
copy windows_scripts\q3cpp.exe  windows\build\ui\
copy windows_scripts\q3rcc.exe  windows\build\ui\
copy windows_scripts\q3asm.exe  windows\build\ui\
copy windows_scripts\ui.q3asm windows\build\

set LIBRARY=
set INCLUDE=
set CODEDIR=..\..\..\..\code

set cc=lcc -DMISSIONPACK -DQ3_VM -S -Wf-target=bytecode -Wf-g -I%CODEDIR%\ui -I%CODEDIR%\qcommon -I%CODEDIR%\game %1

cd windows\build\ui

%cc%  %CODEDIR%\ui\ui_atoms.c
%cc%  %CODEDIR%\ui\ui_gameinfo.c
%cc%  %CODEDIR%\ui\ui_main.c
%cc%  %CODEDIR%\ui\ui_players.c
%cc%  %CODEDIR%\ui\ui_shared.c

copy  %CODEDIR%\ui\ui_syscalls.asm ..

%cc%  %CODEDIR%\game\bg_lib.c
%cc%  %CODEDIR%\game\bg_misc.c

%cc%  %CODEDIR%\qcommon\q_math.c
%cc%  %CODEDIR%\qcommon\q_shared.c

q3asm -f ..\ui

cd ..\..\..\windows_scripts
pause
