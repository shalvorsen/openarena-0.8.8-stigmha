cd ..
mkdir windows
mkdir windows\build
mkdir windows\baseoa
mkdir windows\baseoa\vm
mkdir windows\build\cgame
copy /Y windows_scripts\lcc.exe  windows\build\cgame\
copy /Y windows_scripts\q3cpp.exe  windows\build\cgame\
copy /Y windows_scripts\q3rcc.exe  windows\build\cgame\
copy /Y windows_scripts\q3asm.exe  windows\build\cgame\
copy /Y windows_scripts\cgame.q3asm windows\build\

set LIBRARY=
set INCLUDE=
set CODEDIR=..\..\..\..\code

set cc=lcc -DQ3_VM -S -Wf-target=bytecode -Wf-g -I%CODEDIR%\cgame -I%CODEDIR%\qcommon -I%CODEDIR%\game %1

cd windows\build\cgame

%cc%  %CODEDIR%\cgame\cg_challenges.c
%cc%  %CODEDIR%\cgame\cg_consolecmds.c
%cc%  %CODEDIR%\cgame\cg_draw.c
%cc%  %CODEDIR%\cgame\cg_drawtools.c
%cc%  %CODEDIR%\cgame\cg_effects.c
%cc%  %CODEDIR%\cgame\cg_ents.c
%cc%  %CODEDIR%\cgame\cg_event.c
%cc%  %CODEDIR%\cgame\cg_info.c
%cc%  %CODEDIR%\cgame\cg_localents.c
%cc%  %CODEDIR%\cgame\cg_main.c
%cc%  %CODEDIR%\cgame\cg_marks.c
%cc%  %CODEDIR%\cgame\cg_newdraw.c
%cc%  %CODEDIR%\cgame\cg_particles.c
%cc%  %CODEDIR%\cgame\cg_players.c
%cc%  %CODEDIR%\cgame\cg_playerstate.c
%cc%  %CODEDIR%\cgame\cg_predict.c
%cc%  %CODEDIR%\cgame\cg_scoreboard.c
%cc%  %CODEDIR%\cgame\cg_servercmds.c
%cc%  %CODEDIR%\cgame\cg_snapshot.c
%cc%  %CODEDIR%\cgame\cg_unlagged.c
%cc%  %CODEDIR%\cgame\cg_view.c
%cc%  %CODEDIR%\cgame\cg_weapons.c

%cc%  %CODEDIR%\game\bg_lib.c
%cc%  %CODEDIR%\game\bg_misc.c
%cc%  %CODEDIR%\game\bg_pmove.c
%cc%  %CODEDIR%\game\bg_slidemove.c

copy  %CODEDIR%\cgame\cg_syscalls.asm ..

%cc%  %CODEDIR%\qcommon\q_math.c
%cc%  %CODEDIR%\qcommon\q_shared.c

q3asm -f ..\cgame

cd ..\..\..\windows_scripts
pause
