cd ..
mkdir windows
mkdir windows\build
mkdir windows\missionpack
mkdir windows\missionpack\vm
mkdir windows\build\game
copy windows_scripts\lcc.exe  windows\build\game\
copy windows_scripts\q3cpp.exe  windows\build\game\
copy windows_scripts\q3rcc.exe  windows\build\game\
copy windows_scripts\q3asm.exe  windows\build\game\
copy windows_scripts\game_mp.q3asm windows\build\game.q3asm

set LIBRARY=
set INCLUDE=
set CODEDIR=..\..\..\..\code

set cc=lcc -DQ3_VM -DMISSIONPACK -S -Wf-target=bytecode -Wf-g -I%CODEDIR%\game -I%CODEDIR%\qcommon %1

cd windows\build\game

%cc%  %CODEDIR%\game\ai_chat.c
%cc%  %CODEDIR%\game\ai_cmd.c
%cc%  %CODEDIR%\game\ai_dmnet.c
%cc%  %CODEDIR%\game\ai_dmq3.c
%cc%  %CODEDIR%\game\ai_main.c
%cc%  %CODEDIR%\game\ai_team.c
%cc%  %CODEDIR%\game\ai_vcmd.c
%cc%  %CODEDIR%\game\bg_lib.c
%cc%  %CODEDIR%\game\bg_misc.c
%cc%  %CODEDIR%\game\bg_pmove.c
%cc%  %CODEDIR%\game\bg_slidemove.c
%cc%  %CODEDIR%\game\g_active.c
%cc%  %CODEDIR%\game\g_admin.c
%cc%  %CODEDIR%\game\g_arenas.c
%cc%  %CODEDIR%\game\g_bot.c
%cc%  %CODEDIR%\game\g_client.c
%cc%  %CODEDIR%\game\g_cmds.c
%cc%  %CODEDIR%\game\g_cmds_ext.c
%cc%  %CODEDIR%\game\g_combat.c
%cc%  %CODEDIR%\game\g_items.c
%cc%  %CODEDIR%\game\g_main.c
%cc%  %CODEDIR%\game\bg_alloc.c
%cc%  %CODEDIR%\game\g_fileops.c
%cc%  %CODEDIR%\game\g_killspree.c
%cc%  %CODEDIR%\game\g_misc.c
%cc%  %CODEDIR%\game\g_missile.c
%cc%  %CODEDIR%\game\g_mover.c
%cc%  %CODEDIR%\game\g_playerstore.c
rem %cc%  %CODEDIR%\game\g_rankings.c
%cc%  %CODEDIR%\game\g_session.c
%cc%  %CODEDIR%\game\g_spawn.c
%cc%  %CODEDIR%\game\g_svcmds.c
%cc%  %CODEDIR%\game\g_svcmds_ext.c
rem %cc%  %CODEDIR%\game\g_syscalls.c
copy  %CODEDIR%\game\g_syscalls.asm ..
%cc%  %CODEDIR%\game\g_target.c
%cc%  %CODEDIR%\game\g_team.c
%cc%  %CODEDIR%\game\g_trigger.c
%cc%  %CODEDIR%\game\g_unlagged.c
%cc%  %CODEDIR%\game\g_utils.c
%cc%  %CODEDIR%\game\g_vote.c
%cc%  %CODEDIR%\game\g_weapon.c

%cc%  %CODEDIR%\qcommon\q_math.c
%cc%  %CODEDIR%\qcommon\q_shared.c

q3asm -f ..\game

cd ..\..\..\windows_scripts
pause
